#!/usr/bin/python3
##########################################################################
#
# forms.py - forms for the Debian GitLab user/group creation service
#
# Copyright (C) 2017 Philipp Kaluza
# Copyright (C) 2017 Nicolas Dandrimont
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
##########################################################################

try:
    from flask_wtf import FlaskForm
except ImportError:
    # Compat with flask_wtf 0.12
    from flask_wtf import Form as FlaskForm

from wtforms import (StringField, PasswordField, SelectField, validators,
                     ValidationError)
from wtforms.widgets import Input, HTMLString

USERNAME_SUFFIX = '-guest'
TEAM_PATH_PREFIXES = ['pkg-', 'l10n-']


class InputGroupAddon(Input):
    def __init__(self, input_type, suffix):
        super().__init__(input_type)
        self.suffix = suffix

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        kwargs.setdefault('type', self.input_type)
        if 'value' not in kwargs:
            kwargs['value'] = field._value()
        return HTMLString('<input %s><span class="input-group-addon">%s</span>' % (self.html_params(name=field.name, **kwargs), self.suffix))


class NewUserForm(FlaskForm):
    username_prefix = StringField('Username', widget=InputGroupAddon('text', USERNAME_SUFFIX))
    name = StringField('Full name')
    email = StringField('Email address', [
        validators.Email(message='Invalid email address')
    ])
    password = PasswordField('Password', [
        validators.DataRequired(),
    ])
    confirm = PasswordField('Password (repeat)', [
        validators.EqualTo('password', message='Passwords must match')
    ])


class NewTeamForm(FlaskForm):
    name = StringField('Team Name', [validators.Length(max=64)])
    path_prefix = SelectField('Path Prefix', choices=[(i, i) for i in TEAM_PATH_PREFIXES])
    path_name = StringField('Path Name', [validators.Length(max=64)])

    def validate_path(form, field):
        if '/' in field.data:
            raise ValidationError(
                'The path for a team cannot contain a slash.'
            )
