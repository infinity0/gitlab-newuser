#!/usr/bin/python3
##########################################################################
#
# gitlab_self_service - a flask-based web form for requesting a new gitlab
#                       user account or group, allowing additional constraints
#
# Copyright (C) 2017 Philipp Kaluza
# Copyright (C) 2017 Nicolas Dandrimont
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
##########################################################################

from functools import wraps
import os
from urllib.parse import urljoin

from flask import (Flask, Markup, flash, render_template, request, redirect,
                   session, url_for)

from flask_oauthlib.client import OAuth

from .forms import TEAM_PATH_PREFIXES, USERNAME_SUFFIX, NewTeamForm, NewUserForm
from .gitlab_api import GitLabAPIClient, GitLabAPIError


app = Flask(__name__)
app.config.from_object(__name__)
app.config.update({
    'GITLAB_BASE_URL': 'https://salsa.debian.org/',
    'GITLAB_API_TOKEN': '',
    'GITLAB_OAUTH_CONSUMER_KEY': '',
    'GITLAB_OAUTH_CONSUMER_SECRET': '',
    'GITLAB_USER_EXTRA_PARAMS': {
        'admin': False,
        'can_create_group': False,
        'skip_confirmation': False,
        'external': True,
    },
    'GITLAB_GROUP_ROOT': 'teams',
    'GITLAB_GROUP_EXTRA_PARAMS': {
        'request_access_enabled': True,
    },
    'SECRET_KEY': os.urandom(24),
    'CA_BUNDLE': '/etc/ssl/certs/ca-certificates.crt',
})

app.config.from_envvar('GITLAB_SELF_SERVICE_SETTINGS', silent=True)

# SSL_CERT_FILE is used by the Python SSL module
os.environ.setdefault('SSL_CERT_FILE', app.config['CA_BUNDLE'])
# REQUESTS_CA_BUNDLE is used by the requests module
os.environ.setdefault('REQUESTS_CA_BUNDLE', app.config['CA_BUNDLE'])


oauth = OAuth(app)

gitlab = oauth.remote_app(
    'gitlab',
    app_key='GITLAB_OAUTH',
    base_url=app.config['GITLAB_BASE_URL'],
    request_token_params={'scope': 'read_user'},
    request_token_url=None,
    access_token_method='POST',
    access_token_url=urljoin(app.config['GITLAB_BASE_URL'], 'oauth/token'),
    authorize_url=urljoin(app.config['GITLAB_BASE_URL'], 'oauth/authorize'),
)


def get_root_api():
    """Get the GitLab API client object for the root user"""
    return GitLabAPIClient(
        url=urljoin(app.config['GITLAB_BASE_URL'], 'api/v4/'),
        token=app.config['GITLAB_API_TOKEN'],
        mode='impersonate',
    )


def get_user_api():
    """Get the GitLab API client object for the root user"""
    return GitLabAPIClient(
        url=urljoin(app.config['GITLAB_BASE_URL'], 'api/v4/'),
        token=session['gitlab_token'],
        mode='oauth2',
    )


def login_required(view):
    """Redirects to the login page if needed"""
    @wraps(view)
    def decorated_view(*args, **kwargs):
        if not session.get('gitlab_token'):
            return redirect(url_for('login', next=request.url))
        return view(*args, **kwargs)

    return decorated_view


def login_disabled(view):
    """Redirects to the index page if user is authenticated"""
    @wraps(view)
    def decorated_view(*args, **kwargs):
        if session.get('gitlab_token'):
            return redirect(url_for('index'))
        return view(*args, **kwargs)

    return decorated_view


def update_form_from_api_error(form, api_error):
    """Update a form with data from a GitLabAPIError. Flash errors we could not map
    to the form fields"""
    error_msg = api_error.args[0]
    error_data = api_error.args[1]

    if error_data and isinstance(error_data, dict):
        # Replicate errors from GitLab inside the form
        other_errors = []
        for field, errors in error_data.items():
            if hasattr(form, field):
                getattr(form, field).errors.extend(errors)
            else:
                other_errors.extend(errors)
        if other_errors:
            flash(', '.join(other_errors), 'danger')
    else:
        # Could not process structured error data; print the raw message
        flash(error_msg, 'danger')


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/register/guest/', methods=["GET", "POST"])
@login_disabled
def register_guest_page():
    template_name = 'register/guest.html'

    form = NewUserForm(request.form)

    template_params = {
        'form': form,
    }

    # empty or invalid form
    if (request.method == 'GET'
            or (request.method == "POST" and not form.validate())):
        return render_template(template_name, **template_params)

    username = form.username_prefix.data + USERNAME_SUFFIX
    email = form.email.data
    name = form.name.data
    password = form.password.data

    try:
        get_root_api().create_new_user(
            username=username,
            password=password,
            name=name,
            email=email,
            **app.config.get('GITLAB_USER_EXTRA_PARAMS', {}),
        )
    except GitLabAPIError as e:
        update_form_from_api_error(form, e)
        return render_template(template_name, **template_params)
    else:
        flash(
            Markup(render_template(
                'register/guest_success.html',
                username=username,
                email=email,
                gitlab_root=app.config['GITLAB_BASE_URL'],
            )),
            'success',
        )

    return redirect(url_for('index'))


@app.route('/register/team/', methods=["GET", "POST"])
@login_required
def register_team():
    template_name = 'register/team.html'
    form = NewTeamForm(request.form)

    template_params = {
        'form': form,
    }

    # empty or invalid form
    if (request.method == 'GET'
            or (request.method == "POST" and not form.validate())):
        return render_template(template_name, **template_params)

    name = form.name.data
    path = form.path_prefix.data + form.path_name.data

    try:
        new_group = get_root_api().create_new_group(
            name=name,
            path=path,
            **app.config.get('GITLAB_GROUP_EXTRA_PARAMS', {}),
        )
    except GitLabAPIError as e:
        update_form_from_api_error(form, e)
        return render_template(template_name, **template_params)
    else:
        # The API creates the group with the root user as owner by default.
        # Remove it.
        root_user = get_root_api().get_user()
        if root_user['id'] != session['gitlab_user']['id']:
            get_root_api().add_group_member(
                group=new_group['id'],
                user_id=session['gitlab_user']['id'],
                access_level=50,  # Owner
            )

            get_root_api().delete_group_member(
                group=new_group['id'],
                user_id=root_user['id'],
            )

        flash(
            Markup(render_template(
                'register/team_success.html',
                name=name,
                full_path=new_group['full_path'],
                team_url=urljoin(app.config['GITLAB_BASE_URL'],
                                 new_group['full_path']),
            )),
            'success',
        )

    return redirect(url_for('index'))


@app.route('/login')
def login():
    next = request.args.get('next')
    if next:
        session['login_next'] = next

    return gitlab.authorize(callback=url_for('authorized', _external=True))


@app.route('/logout')
def logout():
    session.pop('gitlab_token', None)
    session.pop('gitlab_user', None)
    return redirect(url_for('index'))


@app.route('/login/authorized')
def authorized():
    resp = gitlab.authorized_response()
    if resp is None or resp.get('access_token') is None:
        return 'Access denied: reason=%s error=%s resp=%s' % (
            request.args['error'],
            request.args['error_description'],
            resp
        )

    session['gitlab_token'] = resp['access_token']
    session['gitlab_user'] = get_user_api().get_user()

    next = session.pop('login_next', None)
    if not next:
        next = url_for('index')
    return redirect(next)


@gitlab.tokengetter
def get_gitlab_oauth_token():
    return (session.get('gitlab_token'), '')


if __name__ == '__main__':
    app.run(debug=True)
