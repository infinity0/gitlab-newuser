gitlab\_self\_service
=====================

Self-service tool for the Debian GitLab instance.

 - Allows naming restrictions for guest accounts
 - Allows self-service creation of groups under a main `teams` arborescence.

Initially sponsored by Ghostroute Consulting / Philipp Kaluza.

Dependencies
------------

    apt-get install python3 python3-flask python3-flaskext.wtf python3-oauthlib python3-requests libjs-bootstrap

Flask-OAuthlib is needed but is embedded for now as there is no Debian package
of it yet.

How to run in development
-------------------------

Copy `dev_settings.py` to another file. Override the settings in this new file.
Run the development server with

  GITLAB_SELF_SERVICE_SETTINGS=/full/path/to/settings_file.py ./develop.py

How to deploy in production
---------------------------

    apt-get install libapache2-mod-wsgi-py3

Copy `dev_settings.py` to `wsgi/prod_settings.py` and adapt its contents. Make
especially sure you set SECRET_KEY to a custom value.

Copy the contents of the `wsgi/apache.conf.example` file to your VirtualHost
definition, changing %DIRECTORY% to the directory where this repo was checked
out.

License
-------

Copyright (C) 2017 Philipp Kaluza
Copyright (C) 2017 Nicolas Dandrimont

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

TODO: make sure the web interface contains a link to the locally hosted
      repository version of gitlab\_self\_service to comply with this license.
