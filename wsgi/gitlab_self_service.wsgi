#!/usr/bin/python3

import os
import sys

# Application
sys.path.insert(1, os.path.join(os.path.dirname(__file__), '..', 'src'))

# Embedded dependencies
for dep in ('Flask-OAuthlib-0.9.4',):
    sys.path.insert(1, os.path.join(os.path.dirname(__file__), '..', 'lib',
                                    dep))

# Horrendous hack as Apache's mod_wsgi doesn't support passing environment
# variables.
os.environ['GITLAB_SELF_SERVICE_SETTINGS'] = (
    os.path.join(os.path.dirname(__file__), 'prod_settings.py')
)

from gitlab_self_service.flask_app import app as application

